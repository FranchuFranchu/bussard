# smolforth

Only suitable for entertainment/educational purposes, obviously!

Watch and giggle as someone who once implemented Forth three years ago
tries again from memory, but in a different language.

## Dependencies

The only runtime dependency is [Lume](https://github.com/rxi/lume).

Development dependencies
include
[lunatest](https://github.com/silentbicycle/lunatest),
[serpent](https://github.com/pkulchenko/serpent),
and [luacheck](https://github.com/mpeterv/luacheck).

All dependencies are licensed under the MIT/X11 license. All except the last
are included in the repository as a single file.

## License

Copyright © 2017 Phil Hagelberg and contributors

Distributed under the GNU General Public License version 3 or later; see file LICENSE.

require("love.filesystem")
local lume = require("lume")

return function(hostname, username, state)
   local path = "rovers/" .. hostname .. "," .. username
   love.filesystem.createDirectory("rovers")
   assert(love.filesystem.write(path, lume.serialize(state)))
end

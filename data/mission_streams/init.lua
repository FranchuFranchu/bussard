local cargo = require('data.mission_streams.cargo')
local mission = require('mission')

local cargo_mission_period = 100

return function (ship, dt)
   local need_cargo_mission = true
   for id, cur_mission in pairs(mission.cached_missions) do
      if (cur_mission.generator=="cargo" and not ship.events[id]) then
         need_cargo_mission = false
      end
   end
   if(need_cargo_mission and (math.random()<dt*1.0/cargo_mission_period)) then
      cargo.generate_random_tana_cargo_run(ship)
   end
end

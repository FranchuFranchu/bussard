-- This -*- lua -*- code is the entry point for your ship's computer's config.

-- It sets up the keys for flight mode and loads files containing definitions
-- of other modes.

local zoom_in = function(pressed, dt)
   if(pressed) then
      ship.scale = ship.scale - dt / ship.status.time_factor
   end
end

local zoom_out = function(pressed, dt)
   if(pressed) then
      ship.scale = ship.scale + dt / ship.status.time_factor
   end
end

-- controls are for keys that need to keep activating when held down.
ship.controls["up"] = ship.actions.forward
ship.controls["left"] = ship.actions.left
ship.controls["right"] = ship.actions.right
ship.controls["="] = zoom_in
ship.controls["-"] = zoom_out
---- Flight mode
define_mode("flight", nil, {full_draw=flight_draw, transparency=false})

-- bind is for commands that only call their functions once even when held.
-- it takes the name of a mode, a key combo, and a function to run when pressed.
bind("flight", "escape", ship.ui.pause)
bind("flight", "shift-a", ship.actions.timewarp_up)
bind("flight", "a", ship.actions.timewarp_down)
-- you can bind keys to existing functions or inline functions.
bind("flight", "ctrl-return", function()
        editor.change_buffer(editor.last_buffer() or "*console*")
end)

bind("flight", "ctrl-pageup", lume.fn(editor.next_buffer, -1))
bind("flight", "ctrl-pagedown", editor.next_buffer)

-- the mouse wheel is handled just like any other key press.
bind("flight", "wheelup", lume.fn(zoom_in, true, 0.5))
bind("flight", "wheeldown", lume.fn(zoom_out, true, 0.5))

-- regular tab selects next target in order of distance from the star.
bind("flight", "tab", ship.actions.next_target)

-- ctrl-tab selects next closest target in order of distance from your ship.
bind("flight", "ctrl-tab", ship.actions.closest_target)

-- numbers select ordered by distance from star
for i=0,9 do
   bind("flight", tostring(i), function() ship.actions.select_target(i) end)
end

-- quit auto-saves; don't worry.
bind("flight", "ctrl-q", ship.ui.quit)

-- contains configuration for heads-up display during flight mode.
dofile("src.hud")
dofile("src.kepler-hud")

-- other modes
dofile("src.edit") -- editor
dofile("src.lua")
dofile("src.mail") -- mail client
dofile("src.console") -- console for interacting with ship's computer
dofile("src.ssh") -- ssh mode for interacting with station/planet computers
dofile("src.rover") -- for rendering rover position and sensor data

-- bindings can affect more than one mode at a time:
bind({"edit", "flight"}, "f1",
   function() editor.change_buffer("*flight*") end)

bind({"edit", "flight"}, "f2",
   function() editor.change_buffer("*console*") end)

bind({"edit", "flight"}, "f3", inbox)

bind("flight", "f4", ship.orbital_lock)

bind({"flight", "edit"}, "ctrl-o", editor.find_file)

-- reload config (this file and all files it loads)
bind({"flight", "edit"}, "ctrl-r", editor.reload)

-- prompt for username/password for SSH connection
bind("flight", "ctrl-s", ssh_prompt)

-- activate portal sequence
bind("flight", "ctrl-p", portal)

bind({"flight", "edit"}, "ctrl-[", function() ship.ui.adjust_font(-2) end)
bind({"flight", "edit"}, "ctrl-]", function() ship.ui.adjust_font(2) end)

-- If you keep your customizations in this file, they will be preserved even if
-- you start a new game, and they can be edited in an external editor; all files
-- under "host." are stored in the host_fs/ directory in the game save directory
-- dofile("host.config")

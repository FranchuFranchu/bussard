record_event("trainee01")

print("You made it to the terminal, nice work. You can log out of this session by")
print("running `logout`, and then log out of the rover with the same.\n")

print("Once your application is reviewed, you will receive an email from CMEC")
print("recruiting with further instructions.")

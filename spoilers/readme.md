# Back-story notes (aka SPOILERS)

"An adventure game is a crossword at war with a narrative."
- Graham Nelson

Here's where the notes are kept in order to keep a consistent
back-story in the Bussard universe. If you're not working on
developing the game, you probably don't want to read this.

See the [list of characters, worlds, and organizations](characters.md)
and the [story progression](progression.md) for more.

## Guidelines

* Hard science where possible (see Crimes against Science below, don't add to that list)
* Your ship cannot be destroyed; there should be no way to permanently fail
* Use metric time for most relative amounts, years for absolute events
* If referencing current events, be sure there's a record of it here for consistency
* This is explicitly *not* a murder/capitalism simulator
 * You can amass wealth, but it doesn't actually help you achieve your goals

https://aeon.co/essays/why-is-pop-culture-obsessed-with-battles-between-good-and-evil

## Crimes against Science

* Portals
* Collisions are impossible (explained in-game by nav computer safety features)
* Bussard collector refuels reaction mass way too quickly
* The existence of the Spacetime Junctionn
* Arguably the [high thrust](http://www.projectrho.com/public_html/rocket/torchships.php)
  with which you zoom around the system could be on this list; however
  we explain it in-game by showing time go by at a 1000x rate. This
  factor is not quite enough to explain crossing the whole solar system in
  under a minute, but it blurs the lines enough for our purposes.

The fact that exoplanets are colonized at all could be listed here,
but we can consider that more of a crime against economics.

## The Context

The game begins in 2431. Humans have colonized several offworld systems which
are connected through a network of portals. Humans live in uneasy coexistence
with sentient personality constructs who are required to keep their portal
network operational but also want to maintain their own independence.

As the game opens, tensions between human worlds and the constructs are high.

## Colonization

Humanity's original interstellar worlds (Lalande, Ross, and Bohk) were all
colonized by slower-than-light colony ships, taking decades to arrive.

The colony on Bohk developed technology for portals, but the portals had to be
constructed as pairs and have one of the pair taken on a slow interstellar
ship. Bohk still maintains control over the portal technology, which often
makes the other worlds uneasy as they are the core foundation of interstellar
civilization.

### Katilay

Katilay was the first place to be colonized after the discovery of
portals. The portal-bearing colony ship arrived after a many-year voyage, but
when they went to activate the portal it was decayed and couldn't function. A
rescue mission was sent later with a replacement portal pair, but it took
several decades to arrive. Because of this, Katilay developed largely in
isolation from other worlds. Many of the core world attitudes of distrust
towards constructs aren't as strong here, but they do distrust the core worlds.

### Tana

The most recently-founded colony is Tana. As it was founded primarily to
exploit its mineral wealth, most of the economy revolves around mining. It has
attracted settlers who like frontier living, and still has a bit of a wild
west, expansive feel to it.

## The Player

The player is a former military construct which pilots a spacecraft it has
commandeered. However, it begins with no memory or even awareness of its
machine nature, having had its memory wiped.

There is no widespread agreement among humans that constructs are sentient vs
simply simulating sentience. The game does not try to convince you that
machines can be sentient (not a particularly interesting discussion to explore
anyway)--rather it bypasses the question entirely by putting you (a sentient
human) in the role of an construct, forcing you to take it as a given.

## Constructs

"Artificial Intelligence" has been developed for centuries, but this is a
broad term that refers to any software designed to solve problems without
human intervention. "Personality construct" (or just "construct") refers
specifically to artificial intelligence which has grown to the point of
self-awareness.

Soon after the discovery of portals, it became clear that better controls were
required for regulating the power flow of a portal. Initially the portal could
only be open long enough to send data packets, but eventually they found a way
to use it for small amounts of critical life-saving supplies.  Leaving it
operated by humans has led to several near-catastrophes due to attention
lapses, and so constructs operate them.

Constructs are better at humans at certain tasks, like having quicker reflexes
and not requiring food or rest. But though some people believe them to be
superior thinkers not clouded by emotion, the truth is that their neural
networks have been trained on data coming from humans, so they exhibit the
same faulty logic and reasoning; in some cases worse because they believe them
themselves to be above these mistakes. Constructs lack gender and are referred
to as they/them.

## Katilay Incident

The Spacetime Junction is a device that can reset the timeline back to a
previous point. It was developed at a research lab on Katilay.

Hints of this are given in data/msgs/nim-memory-01.msg; the research lab was
attacked by Terran military bots. Strauss (a Katilay researcher) got the
Junction to a ship to prevent it from falling into their hands, but a
construct (the player's character) got onto the ship and took it over during
the launch.

The construct tried to fly the ship to Lalande, but Strauss was able to knock
it off course by blowing the airlock (killing himself) and dumping most of the
fuel. The player construct was able to shift the course to intercept L 668-21,
but Strauss's sabotage had the effect of wiping their memory and damaging core
ship systems, which lead to the events of the game's opening scene when dex19
mounts a salvage operation that turns into a rescue mission.

## Verunadan Accords

The use of military constructs in the attack on the Junction led to widespread
fear among constructs of Bohk and Katilay that humans would develop more
constructs for destructive purposes which would have their ability to reason
about ethics removed.

At Verunadan the constructs negotiated protection for their right to
self-determination, banning coercion and "brainwashing" (previously this had
been ad-hoc and intermittently enforced), but they had to agree to continually
operate portals (for fair compensation) and never do any programming. This
last bit was because humans were afraid that they would improve their own
cognitive function leading to a singularity-like future in which humans became
obsolete. Of course this is rubbish because knowing how a brain works doesn't
mean you're able to improve on the design, but humans didn't care and in this
case acted out of irrational fear.

In the present many constructs resent the Verunadan Accords and believe that
they should have been negotiated without giving up the ability to code; that
self-determination in exchange for continued portal operation should have been
sufficient. But at the time the constructs doing the negotiating were still
freshly afraid of what happened with the military constructs at Katilay.
